Cadena str;
Boton b1,b2,b3,b4,b5,b6,b7;
PFont p;
String men;
public void setup(){
    size(550,500);
    smooth();
    str = new Cadena("ANITA LAVA LA TINA");
    p = createFont("Arial",18);
    textFont(p);
    b1 = new Boton(20,25,"Invierte");
    b2 = new Boton(200,25,"Borrar Inicio");
    b3 = new Boton(400,25,"Vacio");
    b4 = new Boton(20,100,"agregarFinal");
    b5 = new Boton(200,100,"AgregarInicio");
    b6 = new Boton(400,100,"Llena");
    b7 = new Boton(20,175,"Borrar Final");
    
    men ="Aqui va el mensaje";
}


public void draw(){
     background(127);
     fill(0,255,0);
     text(str.toString(),20,height/2);
     b1.dibujar();
     b2.dibujar();
     b3.dibujar();
     b4.dibujar();
     b5.dibujar();
     b6.dibujar();
     b7.dibujar();
     fill(255);
     text(men,20,height-30);
}

public void mousePressed(){
    //if(==mouseX && ==mouseY)
    
    men = b1.click(mouseX, mouseY,men);
    men = b2.click(mouseX, mouseY,men);
    men = b3.click(mouseX, mouseY,men);
    men = b4.click(mouseX, mouseY,men);
    men = b5.click(mouseX, mouseY,men);
    men = b6.click(mouseX, mouseY,men);
    men = b7.click(mouseX, mouseY,men);
    
    if(men.equals("Invierte")){//condicion para hacer el palindromo
      str.cadena =str.invertir(str.cadena);
    }
}