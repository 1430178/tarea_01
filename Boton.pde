public class Boton{
    float x;
    float y;
    String mensaje;
    float ancho=120;
    float alto=25;
    public Boton(float a, float b, String c){
        x=a;
        y=b;
        mensaje = c;
    }
    
    public void dibujar(){
        fill(255,255,0);
        rect(x,y,ancho,alto);
        fill(0);
        text(mensaje,x+5,y+20);
    }
    
    public String click(float a, float q, String msj){
        //Boton(a,q,);
        boolean ban;
        ban= isAdentro(a,q);
        if(ban==true){ 
          return mensaje;
        }  
        else
          return  msj;
    }
    
    public boolean isAdentro(float a, float b){
         return ( a >= x && a <=  ( x+ancho ) && b >= y  && b <= (y+alto)   );
    }
    
}